﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

public class TransactionInfo
{
    public string VehicleId { get;  }
    public DateTime transactionTime { get; }
    public decimal Sum { get; }

    public TransactionInfo(string VehicleId, DateTime transactionTime, decimal Sum)
    {
        this.VehicleId = VehicleId;
        this.transactionTime = transactionTime;
        this.Sum = Sum;
    }
}