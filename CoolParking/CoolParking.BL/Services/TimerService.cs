﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;

public class TimerService : ITimerService
{
    Timer timer;
    public TimerService()
    {
        timer = new Timer();
        timer.Elapsed += Elapsed;
    }

    public double Interval {
        get => timer.Interval;
        set {
            timer.Interval = value;
        }
    }

    public event ElapsedEventHandler Elapsed;

    public void Dispose()
    {
        timer.Dispose();
    }

    public void Start()
    {
        timer.Start();
    }

    public void Stop()
    {
        timer.Stop();
    }
}