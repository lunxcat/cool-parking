﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

public class LogService : ILogService
{
    private string filePath = @"c:\temp\MyTest.txt";

    public LogService(string _logFilePath)
    {
        filePath = _logFilePath;
    }

    public string LogPath => filePath;

    public string Read()
    {
        StreamReader file = new StreamReader(filePath);
        try
        {
            return file.ReadToEnd();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            file?.Close();
        }
    }

    public void Write(string logInfo)
    {
        StreamWriter writer = new StreamWriter(filePath, append: true);
        writer.WriteLine(logInfo);
        writer?.Close();
    }
}