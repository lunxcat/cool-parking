﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;

public class ParkingService : IParkingService
{
    List<TransactionInfo> transactionsInfo;
    ITimerService _withdrawTimer;
    ITimerService _logTimer;
    ILogService _logService;

    public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
    {
        Parking.Balance = Settings.initialBalance;

        this._withdrawTimer = _withdrawTimer;
        this._withdrawTimer.Interval = Settings.paymentPeriod;
        this._withdrawTimer.Elapsed += new ElapsedEventHandler(AutoWriteOff);
        this._withdrawTimer.Start();
        this._logTimer = _logTimer;
        this._logTimer.Interval = Settings.writeLogPeriod;
        this._logTimer.Elapsed += new ElapsedEventHandler(AutoWriteTransaction);
        this._logTimer.Start();
        this._logService = _logService;
    }
    // ставити Тр. засоби на Паркінг;
    public void AddVehicle(Vehicle vehicle)
    {
        if (Parking.Vehicles.Count < Settings.parkingCapacity)
        {
            Parking.Vehicles.Add(vehicle);
        }
        else
        {
            throw new System.InvalidOperationException();
        }
    }

    public void Dispose()
    {
        this.Dispose();
        _withdrawTimer.Dispose();
        _logTimer.Dispose();
    }

    public decimal GetBalance()
    {
        return Parking.Balance;
    }

    public int GetCapacity()
    {
        return Settings.parkingCapacity;
    }

    // надавати поточну інформацію про паркомісця (вільно X з Y);
    public int GetFreePlaces()
    {
        return Settings.parkingCapacity - Parking.Vehicles.Count;
    }

    public TransactionInfo[] GetLastParkingTransactions()
    {
        // ??? Good?
        return transactionsInfo.ToArray();
        //throw new System.NotImplementedException();
    }

    public ReadOnlyCollection<Vehicle> GetVehicles()
    {
        return new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
    }

    public string ReadFromLog()
    {
        return _logService.Read();
    }

    // забирати Тр. засоби з Паркінгу;
    public void RemoveVehicle(string vehicleId)
    {
        int index = Parking.Vehicles.FindIndex(v => v.Id == vehicleId);
        if (Parking.Vehicles[index].Balance >= 0)
        {
            Parking.Vehicles.RemoveAt(index);
        }
        else
        {
            throw new System.InvalidOperationException();
        }
    }

    // поповнювати баланс Тр. засобів;
    public void TopUpVehicle(string vehicleId, decimal sum)
    {
        int index = Parking.Vehicles.FindIndex(v => v.Id == vehicleId);
        Parking.Vehicles[index].Balance = Parking.Vehicles[index].Balance + sum;
        /*foreach(Vehicle vehicle in Parking.Vehicles){
            if(vehicle.Id == vehicleId)
            {
                vehicle.Balance += sum;
                return;
            }
        }
        throw new System.ArgumentException();*/
    }

    private void AutoWriteOff(object source, ElapsedEventArgs e)
    {
        foreach (Vehicle vehicle in Parking.Vehicles)
        {
            //Obrahunky
            decimal tarife = 0;
            switch (vehicle.VehicleType)
            {
                case VehicleType.Motorcycle:
                    tarife = Settings.motorcycleTarife;
                    break;
                case VehicleType.PassengerCar:
                    tarife = Settings.carTarife;
                    break;
                case VehicleType.Bus:
                    tarife = Settings.busTarife;
                    break;
                case VehicleType.Truck:
                    tarife = Settings.truckTarife;
                    break;
            }

            decimal sum = 0;
            // Perewirjajemo chy dostatnio groshej dla spysana
            if (vehicle.Balance >= tarife)
            {
                sum = tarife;
            }
            else
            {
                if (vehicle.Balance > 0)
                {
                    sum = vehicle.Balance + ((tarife - vehicle.Balance) * Settings.fineRate);
                }
                else
                {
                    sum = tarife * Settings.fineRate;
                }
            }

            vehicle.Balance -= sum;
            Parking.Balance += sum;

            //Zapys Tranzakci
            TransactionInfo ti = new TransactionInfo(vehicle.Id, DateTime.Now, sum);
            transactionsInfo.Add(ti);
        }
    }

    private void AutoWriteTransaction(object source, ElapsedEventArgs e)
    {
        string result = "";
        foreach (TransactionInfo transactionInfo in transactionsInfo)
        {
            result += transactionInfo.VehicleId + " | " + transactionInfo.transactionTime + " | " + transactionInfo.Sum + "\n"; 
        }
        _logService.Write(result);
        transactionsInfo.Clear();
    }
}