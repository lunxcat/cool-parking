﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.View
{
    public class ConsoleApp
    {
        //public static ParkingService _parkingService;
        //public ParkingService _parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService(@"C:\Users\Niko\Desktop\task\Transactions.log"));

        public static void Main()
        {
            ParkingService _parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService(@"C:\Users\Niko\Desktop\task\Transactions.log"));
            Menu(_parkingService);
        }

        private static void Menu(ParkingService _parkingService)
        {
            MenuTask menuTask = new MenuTask(_parkingService);
            Console.WriteLine("Menu");
            Console.WriteLine("1. Display Current Balance\n2. Display Amount Of Money Earned\n3. Display Free Place\n4. Display Current All Transaction\n5. Display History Transaction\n6. Display Vehicle In The Parking\n7. PutIn The Parking\n 8. Remove Vehicle From Parking\n 9. Replenish Vehicle Balance");
            Console.WriteLine("Select an action");

            int menuItemNumber = 0;
            while (true)
            {
                string vehicleNumberString = Console.ReadLine();
                try
                {
                    menuItemNumber = Int32.Parse(vehicleNumberString);
                }
                catch
                {
                    Console.WriteLine("Error. Please write number! Please try again");
                }

                if (menuItemNumber > 0 && menuItemNumber <= 10)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Error. Enter a number from 1 to 10");
                }
            }

            switch (menuItemNumber)
            {
                case 1:
                    menuTask.DisplayCurrentBalance();
                    break;
                case 2:
                    menuTask.DisplayAmountOfMoneyEarned();
                    break;
                case 3:
                    menuTask.DisplayFreePlace();
                    break;
                case 4:
                    menuTask.DisplayCurrentAllTransaction();
                    break;
                case 5:
                    menuTask.DisplayHistoryTransaction();
                    break;
                case 6:
                    menuTask.DisplayVehicleInTheParking();
                    break;
                case 7:
                    menuTask.PutInTheParking();
                    break;
                case 8:
                    menuTask.RemoveVehicleFromParking();
                    break;
                default:
                    menuTask.ReplenishVehicleBalance();
                    break;
            }
            Menu(_parkingService);
        }
    }
    
    public class MenuTask
    {
        ParkingService _parkingService;
        public MenuTask(ParkingService _parkingService)
        {
            this._parkingService = _parkingService;
        }
        // Вивести на екран поточний баланс Паркінгу.
        public void DisplayCurrentBalance()
        {
            Console.WriteLine("Balance: " + Parking.Balance.ToString());
        }

        // Вивести на екран суму зароблених коштів за поточний період (до запису у лог).
        public void DisplayAmountOfMoneyEarned()
        {
            decimal sum = 0;
            TransactionInfo[] transactionsInfo = _parkingService.GetLastParkingTransactions();
            foreach (TransactionInfo ti in transactionsInfo)
            {
                sum += ti.Sum;
            }
            Console.WriteLine("Earned money : " + Parking.Balance.ToString());
        }

        // Вивести на екран кількість вільних місць на паркуванні(вільно X з Y).
        public void DisplayFreePlace()
        {
            Console.WriteLine("Free place : " + _parkingService.GetFreePlaces());
        }

        // Вивести на екран усі Транзакції Паркінгу за поточний період(до запису у лог).
        public void DisplayCurrentAllTransaction()
        {
            string transactionInfo = "";
            TransactionInfo[] transactionsInfo = _parkingService.GetLastParkingTransactions();
            foreach (TransactionInfo ti in transactionsInfo)
            {
                transactionInfo += ti.VehicleId;
                transactionInfo += " | ";
                transactionInfo += ti.transactionTime;
                transactionInfo += " | ";
                transactionInfo += ti.Sum;
                transactionInfo += "\n";
            }
            Console.WriteLine("Last transaction information");
            Console.WriteLine(transactionInfo);
        }

        // Вивести на екран історію Транзакцій(зчитавши дані з файлу Transactions.log).
        public void DisplayHistoryTransaction()
        {
            Console.WriteLine("Transaction information");
            Console.WriteLine(_parkingService.ReadFromLog());
        }

        // Вивести на екран список Тр.засобів , що знаходяться на Паркінгу.
        public void DisplayVehicleInTheParking()
        {
            Console.WriteLine("Vehicle in the parking");
            int i = 1;
            foreach (Vehicle vehicle in _parkingService.GetVehicles())
            {
                Console.WriteLine(i.ToString() + ". Type: " + vehicle.VehicleType + " | Id: " + vehicle.Id);
                i++;
            }
        }

        // Поставити Транспортний засіб на Паркінг.
        public void PutInTheParking()
        {
            string Id = "";
            Console.WriteLine("Write vehicle ID");
            while (true)
            {
                Id = Console.ReadLine();
                break;
            }

            Console.WriteLine("Select vehicle type number.\n 1. Motorcycle\n 2. PassengerCar\n 3. Bus\n 4. Truck");
            int vehicleNumber = 0;
            VehicleType vehicleType;

            while (true)
            {
                string numberString = Console.ReadLine();
                try
                {
                    vehicleNumber = Int32.Parse(numberString);
                }
                catch
                {
                    Console.WriteLine("Error. Please write number!");
                }

                if (vehicleNumber > 0 && vehicleNumber <= 4)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Error. Enter a number from 1 to 4");
                }
            }

            switch (vehicleNumber)
            {
                case 1:
                    vehicleType = VehicleType.Motorcycle;
                    break;
                case 2:
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case 3:
                    vehicleType = VehicleType.Bus;
                    break;
                default:
                    vehicleType = VehicleType.Truck;
                    break;
            }

            decimal balanse = 0;
            Console.WriteLine("Enter the balance");
            while (true)
            {
                string balanseString = Console.ReadLine();
                try
                {
                    balanse = Int32.Parse(balanseString);
                }
                catch
                {
                    Console.WriteLine("Error. Please write number! Please try again");
                }

                if (balanse > 0)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("You cannot translate a negative value! Please try again");
                }
            }


            Vehicle vehicle = new Vehicle(Id, vehicleType, balanse);
            _parkingService.AddVehicle(vehicle);
            Console.WriteLine("Vehicle add success!");
        }

        // Забрати Транспортний засіб з Паркінгу.
        public void RemoveVehicleFromParking()
        {
            Console.WriteLine("Enter the number of the car you want to pick up");
            int vehicleNumber = selectVehicleWidget();

            _parkingService.RemoveVehicle(Parking.Vehicles[vehicleNumber - 1].Id);
            Console.WriteLine("Vehicle remove!");
        }

        // Поповнити баланс конкретного Тр. засобу.
        public void ReplenishVehicleBalance()
        {
            Console.WriteLine("Enter the number of the car you want replenish the balance");
            int vehicleNumber = selectVehicleWidget();

            decimal amount = 0;
            Console.WriteLine("Enter the top-up amount");
            while (true)
            {
                string amountString = Console.ReadLine();
                try
                {
                    amount = Int32.Parse(amountString);
                }
                catch
                {
                    Console.WriteLine("Error. Please write number! Please try again");
                }
                if (amount > 0)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("You cannot translate a negative value! Please try again");
                }
            }

            _parkingService.TopUpVehicle(Parking.Vehicles[vehicleNumber - 1].Id, amount);
        }

        private int selectVehicleWidget()
        {
            int vehicleNumber = 0;
            while (true)
            {
                string vehicleNumberString = Console.ReadLine();
                try
                {
                    vehicleNumber = Int32.Parse(vehicleNumberString);
                }
                catch
                {
                    Console.WriteLine("Error. Please write number! Please try again");
                }

                if (vehicleNumber > 0 && vehicleNumber <= Parking.Vehicles.Count + 1)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Error. Enter a number from 1 to " + (Parking.Vehicles.Count + 1).ToString());
                }
            }
            return vehicleNumber;
        }
    }
}
